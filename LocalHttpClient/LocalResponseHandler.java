package com.shilupan.panbao.riskmodel.httpclient;

/**
 * 解析时帮助
 *
 * @author pacey
 */
public abstract class LocalResponseHandler {

    protected String uriId;

    protected long startTime = System.currentTimeMillis();

    public String getUriId() {
        return uriId;
    }

    public void setUriId(String uriId) {
        this.uriId = uriId;
    }
}
